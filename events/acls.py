from sre_parse import State
import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json


def get_location_image(state):
    headers = {"Authorization": PEXELS_API_KEY}
    url = "https://api.pexels.com/v1/search?query=" + state
    # payload = {"query": state, "total_results": 1, "per_page": 1}
    # response = requests.request("GET", url, params=payload, headers=headers)
    response = requests.get(url, headers=headers)
    data = json.loads(response.content)
    # print(json.dumps(data))
    pic = data["photos"][0]["url"]
    return pic

    # return "https://www.snowskool.com/uploads/images/Intermediate_to_Advanced_Boarding.jpg"

    # Create a dictionary for the headers to use in the request
    # Create the URL for the request with the city and state
    # Make the request
    # Parse the JSON response
    # Return a dictionary that contains a `picture_url` key and
    #   one of the URLs for one of the pictures in the response


def get_weather_data(city, state):
    url = (
        "http://api.openweathermap.org/geo/1.0/direct?q="
        + city
        + ","
        + state
        + "usa&appid="
        + OPEN_WEATHER_API_KEY
    )
    response = requests.get(url)
    data = json.loads(response.content)[0]
    lon = data["lon"]
    lat = data["lat"]
    lon = str(lon)
    lat = str(lat)

    url2 = (
        "https://api.openweathermap.org/data/2.5/weather?lat="
        + lat
        + "&lon="
        + lon
        + "&appid="
        + OPEN_WEATHER_API_KEY
        + "&units=imperial"
    )
    response2 = requests.get(url2)
    weather_data = json.loads(response2.content)
    weather = {
        "temperature": weather_data["main"]["temp"],
        "description": weather_data["weather"][0]["description"],
    }
    return weather
